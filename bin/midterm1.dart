import 'dart:io';

void postfix(List input) {
  List<int> values = [];
  int left = 0;
  int right = 0;
  for (int i = 0; i < input.length; i++) {
    if (input[i] != '+' &&
        input[i] != '-' &&
        input[i] != '*' &&
        input[i] != '/') {
      values.add(input[i]);
    } else {
      right = values.last;
      values.removeLast();
      left = values.last;
      values.removeLast();
      if (input[i] == '+') {
        values.add(left + right);
      } else if (input[i] == '-') {
        values.add(left - right);
      } else if (input[i] == '*') {
        values.add(left * right);
      } else if (input[i] == '/') {
        values.add(left ~/ right);
      }
    }
  }
  for (int i = 0; i < values.length; i++) {
    print(values[i]);
  }
}

void main(List<String> arguments) {
  List input = [4, 5, 7, 2, '+', '-', '*'];
  postfix(input);
}

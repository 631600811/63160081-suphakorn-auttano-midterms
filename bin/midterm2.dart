import 'dart:io';

void infixtopostfix(List input) {
  List operator = [];
  List postfix = [];

  for (int i = 0; i < input.length; i++) {
    if (input[i] != '+' &&
        input[i] != '-' &&
        input[i] != '*' &&
        input[i] != '/' &&
        input[i] != '(' &&
        input[i] != ')') {
      postfix.add(input[i]);
    } else {
      while (!operator.isEmpty &&
          operator.last != '(' &&
          input[i] != '*' &&
          input[i] != '/') {
        postfix.add(operator.last);
        operator.removeLast();
      }
      operator.add(input[i]);
    }
    if (input[i] == '(') {
      operator.add(input[i]);
    } else if (input[i] == ')') {
      while (operator.last != '(') {
        postfix.add(operator.last);
        operator.removeLast();
      }
      operator.remove('(');
    }
  }
  while (!operator.isEmpty) {
    postfix.add(operator.last);
    operator.removeLast();
  }
  for (int i = 0; i < postfix.length; i++) {
    stdout.write(postfix[i]);
  }
}

void main(List<String> arguments) {
  List input = [1, '+', 2, '+', 3, '+', 4];
  infixtopostfix(input);
}

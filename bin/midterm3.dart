import 'dart:io';

void calpostfix(String input) {
  List<int> values = [];
  int left = 0;
  int right = 0;
  for (int i = 0; i < input.length; i++) {
    if (input[i] != '+' &&
        input[i] != '-' &&
        input[i] != '*' &&
        input[i] != '/') {
      values.add(int.parse(input[i]));
    } else {
      right = values.last;
      values.removeLast();
      left = values.last;
      values.removeLast();
      if (input[i] == '+') {
        values.add(left + right);
      } else if (input[i] == '-') {
        values.add(left - right);
      } else if (input[i] == '*') {
        values.add(left * right);
      } else if (input[i] == '/') {
        values.add(left ~/ right);
      }
    }
  }
  for (int i = 0; i < values.length; i++) {
    print(values[i]);
  }
}

void main(List<String> args) {
  String Input = '47+65++';
  calpostfix(Input);
}
